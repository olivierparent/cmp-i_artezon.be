<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

/**
 * Hasht wachtwoord met het Blowfish hashalgoritme.
 *
 * Zie ook: http://php.net/mcrypt_create_iv
 * Zie ook: http://php.net/base64_encode
 * Zie ook: http://php.net/strtr
 * Zie ook: http://php.net/sprintf
 * Zie ook: http://php.net/crypt
 *
 * @param string $password
 * @return string
 */
function hashPassword($password)
{
    $algo       = '2y'; // Blowfish algoritme (heeft PHP 5.3.7+ nodig)
    $cost       = 13;   // int van 4 t.e.m. 31. Hoe hoger hoe langer de hashberekening duurt
    $randomSalt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');

    if (is_string($randomSalt) && strlen($randomSalt) > 22) {
        $salt = '$' . $algo . '$' . sprintf('%02d', $cost) . '$' . $randomSalt;
        $hashedPassword = crypt($password, $salt);
    } else {
        // Script moet stoppen want $randomSalt is onveilig.
        exit;
    }

    return $hashedPassword;
}

/**
 * Verifieer of het wachtwoord overeenkomt met de berekende hashcode.
 *
 * Zie ook: http://php.net/crypt
 *
 * @param string $password
 * @param string $hashedPassword
 * @return bool
 */
function verifyPassword($password, $hashedPassword)
{
    /**
     * Crypt geeft dezelfde hashcode terug als het wachtwoord met de hashcode als salt gehasht wordt.
     */
    return crypt($password, $hashedPassword) === $hashedPassword;
}

/**
 * Tests
 */
//{
//    $password       = 'Lorem';
//    $hashedPassword = hashPassword($password);
//    $verified       = verifyPassword($password, $hashedPassword);
//    $verifiedBad    = verifyPassword('lorem', $hashedPassword);
//
//    echo '<h1>Wachtwoord</h1>'; var_dump($password);
//    echo '<h1>Gehasht wachtwoord</h1>'; var_dump($hashedPassword);
//    echo '<h1>Verificatie met correct wachtwoord</h1>'; var_dump($verified);
//    echo '<h1>Verificatie met foutief wachtwoord</h1>'; var_dump($verifiedBad);
//    exit;
//}
