<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

const VALIDATOR_EMAIL      = 'VALIDATOR_EMAIL';
const VALIDATOR_DATE       = 'VALIDATOR_DATE';
const VALIDATOR_IDENTICAL  = 'VALIDATOR_IDENTICAL';
const VALIDATOR_LENGTH_MAX = 'VALIDATOR_LENGTH_MAX';
const VALIDATOR_LENGTH_MIN = 'VALIDATOR_LENGTH_MIN';
const VALIDATOR_REQUIRED   = 'VALIDATOR_REQUIRED';

$validationResults = null;

/**
 * Valideer het formulier dat via POST verstuurd is.
 *
 * Zie: php.net/filter_var
 * Zie: php.net/DateTime
 *
 * @param array $validationRules
 * @return bool|array Geeft TRUE of een array met fouten terug.
 */
function isValidPost(array $validationRules)
{
    $validationResults = $validationRules;

    foreach ($validationRules as $elementName => $rules) {
        $elementValue = isset($_POST[$elementName]) ?  $_POST[$elementName] : null;
        foreach ($rules as $ruleName => $ruleValue) {
            switch ($ruleName) {
                case VALIDATOR_EMAIL:
                    $result =
                        (bool) filter_var($elementValue, FILTER_VALIDATE_EMAIL)
                    ;
                    break;
                case VALIDATOR_DATE:
                    $format = 'Y-m-d';
                    $date = DateTime::createFromFormat($format, $elementValue); // Geeft DateTime-object of FALSE terug.
                    $result =
                        $date && $date->format($format) === $elementValue
                    ;
                    break;
                case VALIDATOR_IDENTICAL:
                    $result =
                        $elementValue === $_POST[$ruleValue]
                    ;
                    break;
                case VALIDATOR_LENGTH_MAX:
                    $result =
                        is_string($elementValue) && strlen($elementValue) <= $ruleValue
                    ;
                    break;
                case VALIDATOR_LENGTH_MIN:
                    $result =
                        is_string($elementValue) && strlen($elementValue) >= $ruleValue
                    ;
                    break;
                case VALIDATOR_REQUIRED:
                    $result = ( isset($elementValue) && !is_string($elementValue) )
                           || ( isset($elementValue) &&  is_string($elementValue) && strlen($elementValue) )
                    ;
                    /**
                     * Indien FALSE, alle volgende rules op FALSE zetten en de loop breken.
                     */
                    if (!$result) {
                        foreach ($rules as $ruleName2 => $ruleValue2) {
                            $validationResults[$elementName][$ruleName2] = false;
                        }
                        break 2; // Bovenliggende foreach-lus afbreken.
                    }
                    break;
                default:
                    $result = false;
                    break;
            }
            $validationResults[$elementName][$ruleName] = $result;
        }
    }

    $hasValidationErrors = hasValidationErrors($validationResults);

    return ($hasValidationErrors === false) ? true : $hasValidationErrors;
}

/**
 * Kijkt of er een FALSE in de array $validationResults staat. Zo ja geeft die array terug, zo niet geeft TRUE terug
 *
 * @param $validationResults
 * @param string $elementName
 * @return bool|array
 */
function hasValidationErrors($validationResults = null, $elementName = null)
{
    if ($validationResults === null || $validationResults === true) {

        return false;
    } else {
        if ($elementName !== null) {
            if (isset($validationResults[$elementName])) {
                $rules = $validationResults[$elementName];
                foreach ($rules as $result) {
                    if ($result === false) {
                        return $validationResults[$elementName]; // Has validation errors
                    }
                }
                return false; // No validation errors;
            } else {
                return true; // Has validation errors
            }
        } else {
            foreach ($validationResults as $rules) {
                foreach ($rules as $result) {
                    if ($result === false) {
                        return $validationResults; // Has validation errors
                    }
                }
            }
            return false; // No validation errors
        }
    }
}

/**
 * De validatiefouten omzetten naar validatieboodschappen.
 *
 * @param bool|array $validationResults
 * @param array $validationRules
 * @param string $elementName
 * @return string
 */
function getValidationErrorMessage($validationResults, $validationRules, $elementName)
{
    $elementRules = hasValidationErrors($validationResults, $elementName);
    $message = '';
    if (is_array($elementRules)) {
        foreach ($elementRules as $resultName => $resultValue) {
            if ($resultValue === false) {
                switch ($resultName) {
                    case VALIDATOR_EMAIL:
                        $message .= 'Geef een geldig e-mailadres in. ';
                        break;
                    case VALIDATOR_DATE:
                        $message .= 'Geef een geldige datum in. ';
                        break;
                    case VALIDATOR_IDENTICAL:
                        $message .= 'De waarde moet identiek zijn. ';
                        break;
                    case VALIDATOR_LENGTH_MAX:
                        $message .= sprintf('De waarde mag niet langer dan %s tekens zijn. ', $validationRules[$elementName][$resultName]);
                        break;
                    case VALIDATOR_LENGTH_MIN:
                        $message .= sprintf('De waarde mag niet korter dan %s tekens zijn. ', $validationRules[$elementName][$resultName]);
                        break;
                    case VALIDATOR_REQUIRED:
                        $message .= 'Deze waarde is verplicht. ';
                        break;
                    default:
                        break;
                }
            }
        }
    }
    return $message;
}

/**
 * @param bool|array $validationResults
 * @param array|string $elementNames
 * @return string
 */
function formHelperHasValidationErrors($validationResults, $elementNames)
{
    if ($validationResults !== null) {
        if (!is_array($elementNames)) {
            $elementNames = [$elementNames];
        }

        foreach ($elementNames as $elementName) {
            $hasValidationErrors = hasValidationErrors($validationResults, $elementName);
            if ($hasValidationErrors) {
                break; // Breek foreachloop af.
            }
        }

        if ($hasValidationErrors !== null) {
            return ($hasValidationErrors === false) ? ' has-success' : ' has-error';
        }
    }
}

/**
 * Na POST de waarde van geposte variablen weergeven in inputveld.
 *
 * @param string $elementName
 * @param string $defaultValue Waarde vooraf uit database opgehaald.
 * @return string
 */
function formHelperHasValue($elementName, $defaultValue = '')
{
    return isset($_POST[$elementName]) ? $_POST[$elementName] : $defaultValue;
}

/**
 * Na POST de aangevinkte radiobuttons en checkboxes een 'checked' HTML-attribuut geven.
 *
 * @param string $elementName
 * @param mixed $elementValue
 * @param null $defaultValue Waarde vooraf uit database opgehaald.
 * @return string
 */
function formHelperIsChecked($elementName, $elementValue, $defaultValue = null)
{
    if (isset($_POST[$elementName])) {
        $isChecked = ($elementValue === $_POST[$elementName]);
    } elseif ($defaultValue !== null) {
        $isChecked = ($elementValue === $defaultValue);
    } else {
        $isChecked = false;
    }

    return $isChecked ? ' checked' : '';
}

/**
 * Na POST de geslecteerde opties met een 'selected' HTML-attribuut geven.
 *
 * @param string $elementName
 * @param mixed $elementValue
 * @param null $defaultValue Waarde vooraf uit database opgehaald.
 * @return string
 */
function formHelperIsSelected($elementName, $elementValue, $defaultValue = null)
{
    if (isset($_POST[$elementName])) {
        $isSelected = ($elementValue === $_POST[$elementName]);
    } elseif($defaultValue !== null) {
        $isSelected = ($elementValue === $defaultValue);
    } else {
        $isSelected = false;
    }

    return $isSelected ? ' selected' : '';
}
