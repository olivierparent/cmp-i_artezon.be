<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

return [
//    'da' => 'Dansk &ndash; Deens',
    'de' => 'Deutsch &ndash; Duits',
    'en' => 'English &ndash; Engels',
//    'gr' => 'Eλληνικά  &ndash; Grieks',
//    'es' => 'Espa&ntilde;ol  &ndash; Spaans',
//    'it' => 'Italiano  &ndash; Italiaans',
    'fr' => 'Français  &ndash; Frans',
    'nl' => 'Nederlands',
//    'no' => 'Norsk  &ndash; Noors',
//    'pt' => 'Portugu&ecirc;s &ndash; Portugees',
//    'fi' => 'Suomi  &ndash; Fins',
//    'se' => 'Svenska  &ndash; Zweeds',
];
