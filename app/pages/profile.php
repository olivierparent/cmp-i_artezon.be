<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once appPath() . 'authentication.php';
allowAuthenticatedOnly();

require_once appPath() . 'database.php';
require_once appPath() . 'form.php';
require_once appPath() . 'utilities.php';

$languages = require appPath() . 'languages.php';
$customer_id = $_SESSION['customer']['id'];

if (isset($_POST['btn-profile'])) {
    /**
     * Regels waaraan elke input moet voldoen.
     */
    $validationRules = [
        'givenname' => [
            VALIDATOR_REQUIRED  => null,
            VALIDATOR_LENGTH_MIN =>  2,
            VALIDATOR_LENGTH_MAX => 45,
        ],
        'familyname' => [
            VALIDATOR_REQUIRED => null,
            VALIDATOR_LENGTH_MIN =>  2,
            VALIDATOR_LENGTH_MAX => 45,
        ],
        'language' => [
            VALIDATOR_REQUIRED => null,
        ],
        'street' => [
            VALIDATOR_REQUIRED => null,
            VALIDATOR_LENGTH_MIN =>   2,
            VALIDATOR_LENGTH_MAX => 255,
        ],
        'number' => [
            VALIDATOR_REQUIRED => null,
            VALIDATOR_LENGTH_MIN => 1,
            VALIDATOR_LENGTH_MAX => 5,
        ],
        'city' => [
            VALIDATOR_REQUIRED => null,
        ],
    ];

    $validationResults = isValidPost($validationRules);
    if ($validationResults === true) {
        var_dump($_POST);
        try {
            $sql_customers
                = 'UPDATE `customers` '
                . 'SET '
                .     '`customer_givenname` = :customer_givenname, '
                .     '`customer_familyname` = :customer_familyname, '
                .     '`customer_language` = :customer_language, '
                .     '`customer_updated` = CURRENT_TIMESTAMP '
                . 'WHERE `customer_id` = :customer_id'
            ;

            $db = getDbConnection(); // Databaseconnectie openen.

            $stmt_customers = $db->prepare($sql_customers);
            if ($stmt_customers) {
                $stmt_customers->bindValue(':customer_givenname' , $_POST['givenname']);
                $stmt_customers->bindValue(':customer_familyname', $_POST['familyname']);
                $stmt_customers->bindValue(':customer_language'  , $_POST['language']);
                $stmt_customers->bindValue(':customer_id'        , $customer_id);
                $stmt_customers->execute();
            }

            $sql_addresses
                = 'INSERT INTO `addresses` ('
                .     '`address_street`, '
                .     '`address_number`, '
                .     '`city_id`, '
                .     '`customer_id`'
                . ') VALUES ('
                .     ':address_street, '
                .     ':address_number, '
                .     ':city_id, '
                .     ':customer_id'
                . ')'
            ;

            $stmt_addresses = $db->prepare($sql_addresses);
            if ($stmt_addresses) {
                $stmt_addresses->bindValue(':address_street', $_POST['street']);
                $stmt_addresses->bindValue(':address_number', $_POST['number']);
                $stmt_addresses->bindValue(':city_id'       , $_POST['city']);
                $stmt_addresses->bindValue(':customer_id'   , $customer_id);
                $stmt_addresses->execute();
            }

            $db = null; // Databaseconnectie sluiten.

            redirectTo('home'); // Functie uit app/utilities.php
        } catch (PDOException $e) {
            $db = null; // Databaseconnectie sluiten.
            var_dump($e);
            exit;
        }
    }
}

try {
    $sql_customers
        = 'SELECT '
        .     'CASE `customer_gender` '
        .         'WHEN 0 THEN \'female\' '
        .         'ELSE \'male\' '
        .     'END AS `gender`, '
        .     '`customer_givenname` AS `givenname`, '
        .     '`customer_familyname` AS `familyname`, '
        .     '`customer_birthday` AS `birthday`, '
        .     '`customer_language` AS `language` '
        . 'FROM `customers` '
        . 'WHERE `customer_id` = :customer_id '
        . 'LIMIT 1'
    ;

    $db = getDbConnection(); // Databaseconnectie openen.

    /**
     * Zie ook: http://courses.olivierparent.be/php/databases/pdo-php-data-objects/
     */
    if ($stmt_customers = $db->prepare($sql_customers)) {
        $stmt_customers->bindValue(':customer_id', $customer_id);
        if ($stmt_customers->execute()) {
            $customer = $stmt_customers->fetch();
        }
    }

    $address = [
        'street' => null,
        'number' => null,
        'city'   => null,
    ];
    $sql_addresses
        = 'SELECT '
        .     '`address_street` AS street, '
        .     '`address_number` AS number, '
        .     '`city_id` AS city '
        . 'FROM `addresses` '
        . 'WHERE `customer_id` = :customer_id '
        . 'ORDER BY `address_created` DESC '
        . 'LIMIT 1'
    ;
    if ($stmt_addresses = $db->prepare($sql_addresses)) {
        $stmt_addresses->bindValue(':customer_id', $customer_id);
        if ($stmt_addresses->execute()) {
            $address = $stmt_addresses->fetch();
        }
    }

    $sql_cities
        = 'SELECT '
        .      '`city_id` AS `id`, '
        .      '`city_postalcode` AS `postalcode`, '
        .      '`city_name` AS `name`, '
        .      'CASE TRUE '
		.          'WHEN (`city_postalcode` BETWEEN 1000 AND 1100) THEN \'Brussel Hoofdstad\' '
		.          'WHEN (`city_postalcode` BETWEEN 1500 AND 1999) THEN \'Vlaams-Brabant\' '
		.          'WHEN (`city_postalcode` BETWEEN 2000 AND 2999) THEN \'Antwerpen\' '
		.          'WHEN (`city_postalcode` BETWEEN 3000 AND 3499) THEN \'Vlaams-Brabant\' '
		.          'WHEN (`city_postalcode` BETWEEN 3500 AND 3999) THEN \'Limburg\' '
		.          'WHEN (`city_postalcode` BETWEEN 8000 AND 8999) THEN \'West-Vlaanderen\' '
		.          'WHEN (`city_postalcode` BETWEEN 9000 AND 9999) THEN \'Oost-Vlaanderen\' '
        .          'ELSE NULL '
        .      'END AS `province` '
        . 'FROM `cities` '
        . 'WHERE '
        .      '(`city_postalcode` BETWEEN 1000 AND 1100) OR '
        .      '(`city_postalcode` BETWEEN 1500 AND 3999) OR '
        .      '(`city_postalcode` BETWEEN 8000 AND 9999) '
        . 'ORDER BY '
        .      '`province` ASC, '
        .      '`city_postalcode` ASC, '
        .      '`city_name` ASC'
    ;

    /**
     * Zie: http://courses.olivierparent.be/php/databases/pdo-php-data-objects/
     */
    $stmt_cities = $db->prepare($sql_cities);
    if ($stmt_cities) {
        if ($stmt_cities->execute()) {
            $cities = $stmt_cities->fetchAll();
        }
    }

    $db = null; // Databaseconnectie sluiten.

} catch (PDOException $e) {
    $db = null; // Databaseconnectie sluiten.
    var_dump($e);
    exit;
}


?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<?php// include partialPath('menu') ?>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Profiel</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Profiel</h1>
<?php if (isset($error)): ?>
    <div class="col-sm-offset-3 col-sm-9 alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="glyphicon glyphicon-warning-sign"></i> <?=$error ?>
    </div>
<?php endif ?>
    <form action="<?=$_SERVER['REQUEST_URI'] ?>" class="form-horizontal" method="post">
        <fieldset>
            <legend>Personalia</legend>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'givenname') ?>">
                <label for="givenname" class="col-sm-3 control-label">Voornaam</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="givenname" name="givenname" placeholder="Vul je voornaam in." value="<?=formHelperHasValue('givenname', $customer['givenname']) ?>">
                </div>
<?php if (hasValidationErrors($validationResults, 'givenname')): ?>
                <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'givenname') ?></p>
<?php endif ?>
            </div>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'familyname') ?>">
                <label for="familyname" class="col-sm-3 control-label">Familienaam</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="familyname" name="familyname" placeholder="Vul je familienaam in." value="<?=formHelperHasValue('familyname', $customer['familyname']) ?>">
                </div>
<?php if (hasValidationErrors($validationResults, 'familyname')): ?>
                <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'familyname') ?></p>
<?php endif ?>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Geslacht</label>
                <div class="col-sm-9">
                    <div class="radio-inline">
                        <input type="radio" id="gender-male" name="gender" value="male"<?=formHelperIsChecked('gender', 'male', $customer['gender']) ?> disabled>
                        <label for="gender-male">Man</label>
                    </div>
                    <div class="radio-inline">
                        <input type="radio" id="gender-female" name="gender" value="female"<?=formHelperIsChecked('gender', 'female', $customer['gender']) ?> disabled>
                        <label for="gender-female">Vrouw</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="birthday" class="col-sm-3 control-label">Geboortedatum</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="birthday" name="birthday" value="<?=$customer['birthday'] ?>" disabled>
                </div>
            </div>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'language') ?>">
                <label for="language" class="col-sm-3 control-label">Taal</label>
                <div class="col-sm-9">
                    <select class="form-control" name="language" id="language">
<?php foreach ($languages as $languageCode => $languageName): ?>
                        <option value="<?=$languageCode ?>"<?=formHelperIsSelected('language', $languageCode, strtolower($customer['language'])) ?>><?=$languageName ?></option>
<?php endforeach ?>
                    </select>
                </div>
<?php if (hasValidationErrors($validationResults, 'language')): ?>
                <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'language') ?></p>
<?php endif ?>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, ['street', 'number']) ?>">
                <label for="street" class="col-sm-3 control-label">Straat</label>
                <div class="col-sm-5<?=formHelperHasValidationErrors($validationResults, 'street') ?>">
                    <input type="text" class="form-control" id="street" name="street" placeholder="Straat" maxlength="255" value="<?=formHelperHasValue('street', $address['street']) ?>">
<?php if (hasValidationErrors($validationResults, 'street')): ?>
                    <p class="help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'street') ?></p>
<?php endif ?>
                </div>
                <div class="col-sm-4<?=formHelperHasValidationErrors($validationResults, 'number') ?>">
                    <input type="text" class="form-control" id="number" name="number" placeholder="Nummer" maxlength="5" value="<?=formHelperHasValue('number', $address['number']) ?>">
<?php if (hasValidationErrors($validationResults, 'number')): ?>
                    <p class="help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'number') ?></p>
<?php endif ?>
                </div>
            </div>
            <legend>Adres</legend>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'city') ?>">
                <label for="city" class="col-sm-3 control-label">Stad</label>
                <div class="col-sm-9">
                    <select class="form-control" name="city" id="city" size="10">
<?php $province = ''; foreach ($cities as $key => $city): ?>
<?php if ($city['province'] !== $province): ?>
                        <optgroup label="<?=$city['province'] ?>">
<?php endif; $province = $city['province']; ?>
                            <option value="<?=$city['id'] ?>"<?=formHelperIsSelected('city', $city['id'], $address['city']) ?>><?=$city['postalcode'] ?> &mdash; <?=toLowerCase($city['name']) ?></option>
<?php if (!isset($cities[$key + 1]) || (isset($cities[$key + 1]) && $province !== $cities[$key + 1]['province'])): ?>
                        </optgroup>
<?php endif?>
<?php endforeach ?>
                    </select>
                </div>
<?php if (hasValidationErrors($validationResults, 'city')): ?>
                <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'city') ?></p>
<?php endif ?>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary" name="btn-profile">Sla wijzigingen op</button>
                <a href="?page=home" class="btn btn-link">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<?php include partialPath('footer') ?>
</body>
</html>