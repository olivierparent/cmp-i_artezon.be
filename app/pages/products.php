<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once appPath() . 'database.php';
require_once appPath() . 'navigation.php';

$products = [];

if (isset($_GET['category'])) {
    try {
        $sql_products
            = 'SELECT '
            .     '`product_code` AS `code`, '
            .     '`product_name` AS `name`, '
            .     '`product_description` AS `description`, '
            .     '`product_picture` AS `picture`, '
            .     '`price_value` AS `price`, '
            .     '`price_currency` AS `currency` '
            . 'FROM `products` INNER JOIN `prices` USING (`product_id`) '
            . 'WHERE '
            .     '`product_code` LIKE :product_code '
            . 'ORDER BY '
            .     '`product_code` ASC, '
            .     '`price_created` DESC '
        ;

        $db = getDbConnection(); // Databaseconnectie openen.

        $stmt_products = $db->prepare($sql_products);
        if ($stmt_products) {
            switch ($_GET['category']) {
                case 'clothing':
                    $product_code = 'C%';
                    break;
                case 'food_and_drink':
                    $product_code = 'F%';
                    break;
                case 'gadgets':
                    $product_code = 'G%';
                    break;
                case 'toys':
                    $product_code = 'T%';
                    break;
                default:
                    $product_code = '%';
                    break;
            }
            $stmt_products->bindValue(':product_code', $product_code);
            $stmt_products->execute();
            $products = $stmt_products->fetchAll();
        }

        $db = null; // Databaseconnectie sluiten.
    } catch (PDOException $e) {
        $db = null; // Databaseconnectie sluiten.
        var_dump($e);
        exit;
    }

} elseif (isset($_GET['search'])) {
    try {
        $sql_products
            = 'SELECT '
            .     '`product_code` AS `code`, '
            .     '`product_name` AS `name`, '
            .     '`product_description` AS `description`, '
            .     '`product_picture` AS `picture`, '
            .     '`price_value` AS `price`, '
            .     '`price_currency` AS `currency` '
            . 'FROM `products` INNER JOIN `prices` USING (`product_id`) '
            . 'WHERE '
            .     '`product_name` LIKE :search OR '
            .     '`product_description` LIKE :search '
            . 'ORDER BY '
            .     '`product_code` ASC, '
            .     '`price_created` DESC'
        ;

        $db = getDbConnection(); // Databaseconnecite openen

        $stmt_products = $db->prepare($sql_products);
        if ($stmt_products) {
            $searchTerm = $_GET['search'];
            $stmt_products->bindValue(':search', "%{$searchTerm}%");
            $stmt_products->execute();
            $products = $stmt_products->fetchAll();
        }

        $db = null; // Databaseconnectie sluiten.
    } catch (PDOException $e) {
        $db = null; // Databaseconnectie sluiten.
        var_dump($e);
        exit;
    }
}


$pageName = '';
if (isset($_GET['category'])) {
    $menuItems = readMenuJson();
    foreach ($menuItems as $menuItem) {
        if ($menuItem->category === $_GET['category']) {
            $pageName = $menuItem->label;
            break; // Stop foreach loop want we hebben de paginanaam gevonden.s
        }
    }
} elseif (isset($_GET['search'])) {
    $pageName = "Zoeken: <strong>{$_GET['search']}</strong>" ;
}

?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<header>
    <?php include partialPath('menu') ?>
    <div class="hidden-xs hidden-lg">
        <img src="http://lorempixel.com/1920/200/abstract/7" alt="" class="img-responsive">
    </div>
</header>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active"><?=$pageName ?></li>
    </ol>
    <div class="row">
<?php $i = 0; foreach ($products as $product): ?>
        <article class="product col-md-4 clearfix <?=$config['colour'][$i++ % 6]?>">
            <div class="price pull-right"><i class="glyphicon glyphicon-<?=code2Currency($product['currency'])?>"></i> <?=price((float) $product['price']) ?></div>
            <img src="<?=$product['picture'] ?>" height="150" width="150" alt="<?=$product['name'] ?>" class="img-thumbnail">
            <h1><?=$product['name'] ?></h1>
            <p class="pull-left"><?=$product['description'] ?></p>
            <a class="btn btn-default pull-left" href="index.php?page=api&origin=home&action=add&product=<?=$product['code'] ?>"><i class="glyphicon glyphicon-plus-sign"></i> in winkelmandje</a>
        </article>
<?php endforeach ?>
    </div>
</div>
<?php include partialPath('footer') ?>
</body>
</html>
