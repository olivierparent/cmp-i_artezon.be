<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

if (isset($_GET['action']) && isset($_GET['product'])) {

    $productCode = $_GET['product'];
    $amount = isset($_COOKIE['shoppingcart']) && isset($_COOKIE['shoppingcart'][$productCode]) ? (int) $_COOKIE['shoppingcart'][$productCode] : 0;
    $cookieName = "shoppingcart[{$productCode}]";

    /**
     * Maximaal aantal per product is 9
     */
    if (8 < $amount) {
        $amount = 8;
    }

    if ($amount < 0) {
        $amount = 0;
    }

//    var_dump($_COOKIE);
//    var_dump($amount);

    switch ($_GET['action']) {
        case 'add':
            setcookie($cookieName, ++$amount, time() + 3600 * 24 * 7);
            break;
        case 'remove':
            if (2 <= $amount) {
                setcookie($cookieName, --$amount, time() + 3600 * 24 * 7);
            } else {
                /**
                 * Tijd in het verleden verwijdert een cookie.
                 * LET OP: het is de browser die de cookie verwijdert en dus telt de tijd van de client niet de server!
                 */
                setcookie($cookieName, null, time() - 3600);
            }
            break;
        default:
            break;
    }
}

require_once appPath() . 'utilities.php';
if (isset($_GET['origin'])) {
    $page = $_GET['origin'];
    redirectTo($page); // Functie uit app/utilities.php
} else {
    redirectTo('home'); // Functie uit app/utilities.php
}
