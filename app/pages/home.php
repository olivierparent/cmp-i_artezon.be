<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */


require_once appPath() . 'utilities.php';

$welcome = '';
if (isset($_SESSION['customer'])) {
    welcomeMessage($welcome, $_SESSION['customer']); // Functie uit app/utilities.php
} else {
    welcomeMessage($welcome); // Functie uit app/utilities.php
}

require_once appPath() . 'database.php';
try {
    $db = getDbConnection(); // Databaseconnectie openen.

    $sql_products
        = 'SELECT '
        .     '`product_code` AS `code`, '
        .     '`product_name` AS `name`, '
        .     '`product_description` AS `description`, '
        .     '`product_picture` AS `picture`, '
        .     '`price_value` AS `price`, '
        .     '`price_currency` AS `currency` '
        . 'FROM `products` INNER JOIN `prices` USING (`product_id`) '
        . 'ORDER BY '
        .     '`product_id` DESC, '
        .     '`product_code` ASC, '
        .     '`price_created` DESC '
        . 'LIMIT 6'
    ;

    $res_products = $db->query($sql_products);
    $products = ($res_products) ? $res_products->fetchAll() : [];

} catch (PDOException $e) {
    var_dump($e);
    exit;
}

$db = null; // Databaseconnectie sluiten.

?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<header>
<?php include partialPath('menu') ?>
    <div class="hidden-xs hidden-lg">
        <img src="http://lorempixel.com/1920/200/abstract/7" alt="" class="img-responsive">
    </div>
</header>

<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Startpagina</li>
    </ol>

    <div class="jumbotron">
        <h1><?=$welcome ?>!</h1>
        <p>Deze webwinkel is een demoproject voor het <abbr title="Opleingsonderdeel">OLOD</abbr> <strong>Crossmedia Publishing I</strong> van de afstudeerrichting
            <abbr title="Multimediaproductie">MMP</abbr> van de opleiding <strong>Bachelor in de Grafische en digitale media</strong> aan de Arteveldehogeschool.</p>
        <p>In dit project wordt de basis van PHP 5.4 en MySQL gedemonstreerd aan de hand van niet-<abbr title="Objectgeoriënteerde">OO</abbr> code en geldt niet als voorbeeld van hoe een webwinkel voor productie ontworpen moet worden.</p>
    </div>

    <div class="row">
<?php $i = 0; foreach ($products as $product): ?>
        <article class="product col-md-4 clearfix <?=$config['colour'][$i++ % 6]?>">
            <div class="price pull-right"><i class="glyphicon glyphicon-<?=code2Currency($product['currency'])?>"></i> <?=price((float) $product['price']) ?></div>
            <img src="<?=$product['picture'] ?>" height="150" width="150" alt="<?=$product['name'] ?>" class="img-thumbnail">
            <h1><?=$product['name'] ?></h1>
            <p class="pull-left"><?=$product['description'] ?></p>
            <a class="btn btn-default pull-left" href="index.php?page=api&origin=home&action=add&product=<?=$product['code'] ?>"><i class="glyphicon glyphicon-plus-sign"></i> in winkelmandje</a>
        </article>
<?php endforeach ?>
    </div>
</div>
<?php include partialPath('footer') ?>
</body>
</html>
