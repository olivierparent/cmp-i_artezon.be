<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

if (isset($_POST['btn-login'])) {
    try {
        $sql_customers
            = 'SELECT '
            .     '`customer_id`         AS `id`, '
            .     '`customer_password`   AS `password`, '
            .     '`customer_givenname`  AS `givenname`, '
            .     '`customer_familyname` AS `familyname` '
            . 'FROM `customers` WHERE '
            .     '`customer_email` = :customer_email '
            . 'LIMIT 1'
        ;

        require_once appPath() . 'database.php';
        $db = getDbConnection(); // Databaseconnectie openen.

        /**
         * Zie ook: http://courses.olivierparent.be/php/databases/pdo-php-data-objects/
         */
        if ($stmt_customers = $db->prepare($sql_customers)) {
            $stmt_customers->bindValue(':customer_email', $_POST['email']);
            if ($stmt_customers->execute()) {
                if ($customer = $stmt_customers->fetch() ) {
                    require_once appPath() . 'authentication.php';
                    if (attemptAuthentication($customer, $_POST['password'])) { // Functie uit app/authentication.php
                        require_once appPath() . 'utilities.php';
                        redirectTo('home'); // Functie uit app/utilities.php
                    }
                }
            }
        }

        $db = null; // Databaseconnectie sluiten.

    } catch (PDOException $e) {
        $db = null; // Databaseconnectie sluiten.
        var_dump($e);
        exit;
    }
    $error = 'Kan niet aanmelden met deze gegevens.';
}
?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Aanmelden</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Meld je aan</h1>
<?php if (isset($error)): ?>
    <div class="col-sm-offset-3 col-sm-9 alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="glyphicon glyphicon-warning-sign"></i> <?=$error ?>
    </div>
<?php endif ?>
    <form action="<?=$_SERVER['REQUEST_URI'] ?>" class="form-horizontal" method="post" autocomplete="off">
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">E-mailadres</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Vul je e-mailadres in.">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Wachtwoord</label>
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Vul je wachtwoord in.">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary" name="btn-login"><i class="glyphicon glyphicon-log-in"></i> Aanmelden</button>
                <a href="?page=home" class="btn btn-link">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<?php include partialPath('footer') ?>
</body>
</html>
