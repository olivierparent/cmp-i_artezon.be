<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once appPath() . 'authentication.php';
require_once appPath() . 'database.php';
require_once appPath() . 'utilities.php';

allowAuthenticatedOnly();

if (isset($_FILES['csv-cities']) && !$_FILES['csv-cities']['error']) {
    $fileName = $_FILES['csv-cities']['tmp_name'];
    $cities = [];

    /**
     * Zie ook: http://php.net/ini_set
     * Zie ook: http://php.net/fopen
     * Zie ook: http://php.net/fgetcsv
     * Zie ook: http://php.net/fclose
     */
    ini_set('auto_detect_line_endings', '1'); // In het geval deze optie in php.ini niet aan zou staan.
    $file = fopen($fileName, 'r');
    if ($file) {
        while ($fileLine = fgetcsv($file, null, ';')) {
            $cities[] = [
                'postalcode' => $fileLine[0],
                'name'       => $fileLine[1],
            ];
        }
        fclose($file);
    }

    /**
     * Bij encodingproblemen, zie: http://php.net/iconv
     */
    if (!empty($cities)) {
        try {
            $sql_cities
                = 'INSERT INTO `cities` ('
                .     '`city_postalcode`, '
                .     '`city_name`'
                . ') VALUES ('
                .     ':city_postalcode, '
                .     ':city_name'
                . ')'
            ;

            $db = getDbConnection(); // Databaseconnectie openen.

            /**
             * Zie ook: http://courses.olivierparent.be/php/databases/pdo-php-data-objects/
             */
            $stmt_cities = $db->prepare($sql_cities);
            if ($stmt_cities) {
                $insertedRows_cities = 0;
                foreach ($cities as $key => $city) {
                    /**
                     * Het eerste item in onze array bestaat uit kolomtitels uit het CSV-bestand. Deze mogen niet in onze
                     * database terechtkomen. $key is array index, en is een getal vanaf 0. 0 wordt door PHP als FALSE
                     * beschouwd, 1 en hoger als TRUE.
                     */
                    if ($key) {
                        $stmt_cities->bindValue(':city_postalcode', $city['postalcode']);
                        $stmt_cities->bindValue(':city_name'      , $city['name']);
                        $stmt_cities->execute();
                        $insertedRows_cities += $stmt_cities->rowCount();
                    }
                }
            }

            $db = null; // Databaseconnectie sluiten.
        } catch (PDOException $e) {
            $db = null; // Databaseconnectie sluiten.
            var_dump($e);
            exit;
        }
    }
}

if (isset($_FILES['xml-products']) && !$_FILES['xml-products']['error']) {
    $products = [];

    $pathXml = $_FILES['xml-products']['tmp_name'];
    $xml = new DOMDocument();
    $xml->load($pathXml);

    $pathXsd = appPath(['..', 'data']) . 'products.xsd';
    if (file_exists($pathXsd)) {
        $isValid = $xml->schemaValidate($pathXsd);
        if ($isValid) {
            try {
                $sql_products
                    = 'INSERT INTO `products` ('
                    .     '`product_code`, '
                    .     '`product_name`, '
                    .     '`product_description`, '
                    .     '`product_picture`'
                    . ') VALUES ('
                    .     ':code, '
                    .     ':name, '
                    .     ':description, '
                    .     ':picture'
                    . ')'
                ;
                $sql_prices
                    = 'INSERT INTO `prices` ('
                    .     '`price_value`, '
                    .     '`price_currency`, '
                    .     '`product_id`'
                    . ') VALUES ('
                    .     ':value, '
                    .     ':currency, '
                    .     ':product_id'
                    . ')'
                ;

                $db = getDbConnection(); // Databaseconnectie openen.

                $stmt_products = $db->prepare($sql_products);
                $stmt_prices   = $db->prepare($sql_prices);
                if ($stmt_products && $stmt_prices) {
                    $insertedRows_products = 0;

                    $products = new SimpleXMLElement($xml->saveXML());
                    foreach ($products as $product) {
                        $stmt_products->bindValue(':code'       , $product->code);
                        $stmt_products->bindValue(':name'       , $product->name);
                        $stmt_products->bindValue(':description', $product->description);
                        $stmt_products->bindValue(':picture'    , $product->picture->attributes()['source']);
                        $stmt_products->execute();
                        $product_id = $db->lastInsertId(); // PK van laatste insert.

                        $stmt_prices->bindValue(':value'     , (float) $product->price);
                        $stmt_prices->bindValue(':currency'  , $product->price->attributes()['currency']);
                        $stmt_prices->bindValue(':product_id', $product_id);
                        $stmt_prices->execute();

                        $insertedRows_products += $stmt_prices->rowCount();
                    }
                }

                $db = null; // Databaseconnectie sluiten.
            } catch (PDOException $e) {
                $db = null; // Databaseconnectie sluiten.
                switch ($e->getCode()) {
                    case '23000':
                        $error = "Een productcode is al in gebruik.";
                        break;
                    default:
                        $error = 'Er is een fout gebeurd: ' . $e->getMessage();
                        break;
                }
            }

        } else {
            die('Ongeldige XML');
        }
    } else {
        die("Kan <strong>{$pathXsd}</strong> niet vinden om te valideren");
    }
}

?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Beheer</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Beheer</h1>
    <form action="<?=$_SERVER['REQUEST_URI'] ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post" role="form">
<?php if (isset($error)): ?>
        <div class="col-sm-offset-3 col-sm-9 alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="glyphicon glyphicon-warning-sign"></i> <?=$error ?>
        </div>
<?php endif ?>
        <fieldset>
            <legend>Postcodes</legend>
<?php if(isset($insertedRows_cities)): ?>
            <p>Er zijn <span class="badge"><?=$insertedRows_cities ?></span> rijen ingevoerd in de tabel <code class="label label-default">cities</code>.</p>
<?php endif ?>
            <div class="form-group">
                <label for="csv-cities" class="col-sm-3 control-label"><abbr title="Comma Separated Values">CSV</abbr>-bestand:</label>
                <div class="col-sm-9">
                    <input type="file" accept="text/csv" id="csv-cities" name="csv-cities">
                    <small class="help-block">Importeer een CSV-bestand met UTF-8-encoding.</small>
                </div>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default" name="btn-import"><i class="glyphicon glyphicon-import"></i> Importeer</button>
                <a href="?page=home" class="btn btn-link">Terug naar de startpagina</a>
            </div>
        </div>
    </form>

    <form action="<?=$_SERVER['REQUEST_URI'] ?>" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" method="post" role="form">
        <fieldset>
            <legend>Producten</legend>
<?php if(isset($insertedRows_products)): ?>
            <p>Er zijn <span class="badge"><?=$insertedRows_products ?></span> rijen ingevoerd in de tabel <code class="label label-default">products</code>.</p>
<?php endif ?>
            <div class="form-group">
                <label for="xml-products" class="col-sm-3 control-label"><abbr title="Extensible Markup Language">XML</abbr>-bestand:</label>
                <div class="col-sm-9">
                    <input type="file" accept="application/xml" id="xml-products" name="xml-products">
                    <small class="help-block">Importeer een XML-bestand met UTF-8-encoding.</small>
                </div>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-default" name="btn-import"><i class="glyphicon glyphicon-import"></i> Importeer</button>
                <a href="?page=home" class="btn btn-link">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<?php include partialPath('footer') ?>
</body>
</html>
