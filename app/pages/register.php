<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once appPath() . 'form.php';

$languages = require appPath() . 'languages.php';

if (isset($_POST['btn-register'])) {
    /**
     * Regels waaraan elke input moet voldoen.
     */
    $validationRules = [
        'givenname' => [
            VALIDATOR_REQUIRED  => null,
            VALIDATOR_LENGTH_MIN =>  2,
            VALIDATOR_LENGTH_MAX => 45,
        ],
        'familyname' => [
            VALIDATOR_REQUIRED => null,
            VALIDATOR_LENGTH_MIN =>  2,
            VALIDATOR_LENGTH_MAX => 45,
        ],
        'gender'  => [
            VALIDATOR_REQUIRED => null,
        ],
        'birthday'  => [
            VALIDATOR_REQUIRED => null,
            VALIDATOR_DATE => null,
        ],
        'email' => [
            VALIDATOR_REQUIRED => null,
            VALIDATOR_EMAIL => null,
        ],
        'password' => [
            VALIDATOR_REQUIRED => null,
            VALIDATOR_LENGTH_MIN => 8,
        ],
        'password-repeat' => [
            VALIDATOR_REQUIRED => null,
            VALIDATOR_LENGTH_MIN => 8,
            VALIDATOR_IDENTICAL => 'password',
        ],
        'language' => [
            VALIDATOR_REQUIRED => null,
        ],
    ];

    $validationResults = isValidPost($validationRules);
    if ($validationResults === true) {

        try {
            $sql_customers
                = 'INSERT INTO `customers` ('
                .     '`customer_email`, '
                .     '`customer_password`, '
                .     '`customer_gender`, '
                .     '`customer_givenname`, '
                .     '`customer_familyname`, '
                .     '`customer_birthday`, '
                .     '`customer_language`'
                . ') VALUES ('
                .     ':customer_email, '
                .     ':customer_password, '
                .     ':customer_gender, '
                .     ':customer_givenname, '
                .     ':customer_familyname, '
                .     ':customer_birthday, '
                .     ':customer_language'
                . ')'
            ;
//        var_dump($sql_customers); exit;

            require_once appPath() . 'database.php';
            $db = getDbConnection(); // Databaseconnectie openen.

            /**
             * Zie ook: http://courses.olivierparent.be/php/databases/pdo-php-data-objects/
             */
            if ($stmt_customers = $db->prepare($sql_customers)) {
                require_once appPath() . 'security.php';
                $stmt_customers->bindValue(':customer_email'     , $_POST['email']);
                $stmt_customers->bindValue(':customer_password'  , hashPassword($_POST['password'])); // Functie uit app/security.php
                $stmt_customers->bindValue(':customer_gender'    , ($_POST['gender'] === 'male') ? 1 : 0 );
                $stmt_customers->bindValue(':customer_givenname' , $_POST['givenname']);
                $stmt_customers->bindValue(':customer_familyname', $_POST['familyname']);
                $stmt_customers->bindValue(':customer_birthday'  , $_POST['birthday']);
                $stmt_customers->bindValue(':customer_language'  , $_POST['language']);
                $stmt_customers->execute();
            }

            $db = null; // Databaseconnectie sluiten.

            require_once appPath() . 'utilities.php';
            redirectTo('home'); // Functie uit app/utilities.php
        } catch (PDOException $e) {
            $db = null; // Databaseconnectie sluiten.
            switch ($e->getCode()) {
                case '23000':
                    $error = "Er bestaat al een gebruiker met <strong>{$_POST['email']}</strong> als e-mailadres.";
                    break;
                default:
                    $error = 'Er is een fout gebeurd: ' . $e->getMessage();
                    break;
            }
        }
    }
}

?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<?php// include partialPath('menu') ?>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Registreren</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Registreer je</h1>
<?php if (isset($error)): ?>
    <div class="col-sm-offset-3 col-sm-9 alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="glyphicon glyphicon-warning-sign"></i> <?=$error ?>
    </div>
<?php endif ?>
    <form action="<?=$_SERVER['REQUEST_URI'] ?>" class="form-horizontal" method="post" role="form" autocomplete="off">
        <fieldset>
            <legend>Aanmeldgegevens</legend>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'email') ?>">
                <label for="email" class="col-sm-3 control-label">E-mailadres</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Vul je e-mailadres in." value="<?=formHelperHasValue('email') ?>">
                    </div>
                </div>
                <?php if (hasValidationErrors($validationResults, 'email')): ?>
                    <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'email') ?></p>
                <?php endif ?>
            </div>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'password') ?>">
                <label for="password" class="col-sm-3 control-label">Wachtwoord</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Vul je wachtwoord in.">
                    </div>
                </div>
                <?php if (hasValidationErrors($validationResults, 'password')): ?>
                    <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'password') ?></p>
                <?php endif ?>
            </div>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'password-repeat') ?>">
                <label for="password-repeat" class="col-sm-3 control-label">Wachtwoord herhalen</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input type="password" class="form-control" id="password-repeat" name="password-repeat" placeholder="Herhaal je wachtwoord.">
                    </div>
                </div>
                <?php if (hasValidationErrors($validationResults, 'password-repeat')): ?>
                    <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'password-repeat') ?></p>
                <?php endif ?>
            </div>
        </fieldset>

        <fieldset>
            <legend>Personalia</legend>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'givenname') ?>">
                <label for="givenname" class="col-sm-3 control-label">Voornaam</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="givenname" name="givenname" placeholder="Vul je voornaam in." value="<?=formHelperHasValue('givenname') ?>">
                </div>
                <?php if (hasValidationErrors($validationResults, 'givenname')): ?>
                    <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'givenname') ?></p>
                <?php endif ?>
            </div>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'familyname') ?>">
                <label for="familyname" class="col-sm-3 control-label">Familienaam</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="familyname" name="familyname" placeholder="Vul je familienaam in." value="<?=formHelperHasValue('familyname') ?>">
                </div>
                <?php if (hasValidationErrors($validationResults, 'familyname')): ?>
                    <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'familyname') ?></p>
                <?php endif ?>
            </div>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'gender') ?>">
                <label class="col-sm-3 control-label">Geslacht</label>
                <div class="col-sm-9">
                    <div class="radio-inline">
                        <input type="radio" id="gender-male" name="gender" value="male"<?=formHelperIsChecked('gender', 'male') ?>>
                        <label for="gender-male">Man</label>
                    </div>
                    <div class="radio-inline">
                        <input type="radio" id="gender-female" name="gender" value="female"<?=formHelperIsChecked('gender', 'female') ?>>
                        <label for="gender-female">Vrouw</label>
                    </div>
                </div>
                <?php if (hasValidationErrors($validationResults, 'gender')): ?>
                    <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'gender') ?></p>
                <?php endif ?>
            </div>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'birthday') ?>">
                <label for="birthday" class="col-sm-3 control-label">Geboortedatum</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="birthday" name="birthday" value="<?=formHelperHasValue('birthday') ?>">
                </div>
                <?php if (hasValidationErrors($validationResults, 'birthday')): ?>
                    <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'birthday') ?></p>
                <?php endif ?>
            </div>
            <div class="form-group<?=formHelperHasValidationErrors($validationResults, 'language') ?>">
                <label for="language" class="col-sm-3 control-label">Taal</label>
                <div class="col-sm-9">
                    <select class="form-control" name="language" id="language">
                        <option value="" selected>&mdash; Kies je taal &mdash;</option>
                        <?php foreach ($languages as $languageCode => $languageName): ?>
                            <option value="<?=$languageCode ?>"<?=formHelperIsSelected('language', $languageCode)?>><?=$languageName ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <?php if (hasValidationErrors($validationResults, 'language')): ?>
                    <p class="col-sm-offset-3 col-sm-9 help-block"><i class="glyphicon glyphicon-warning-sign"></i> <?=getValidationErrorMessage($validationResults, $validationRules, 'language') ?></p>
                <?php endif ?>
            </div>
        </fieldset>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary" name="btn-register">Registreren</button>
                <a href="?page=home" class="btn btn-link">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<?php include partialPath('footer') ?>
</body>
</html>
