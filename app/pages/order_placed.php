<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once appPath() . 'authentication.php';
allowAuthenticatedOnly();

$orders = [];
$priceTotal = 0;
try {
    $sql_orders
        = 'SELECT '
        .     '`product_name` AS `name`, '
        .     '`product_amount` AS `amount`, '
        .     '`price_currency` AS `currency`, '
        .     '`price_value` AS `price` '
        . 'FROM `orders_has_products` NATURAL JOIN `products` NATURAL JOIN `prices` '
        . 'WHERE `order_id` = ('
        .      'SELECT '
        .          '`order_id` '
        .      'FROM `orders` '
        .      'WHERE '
        .          '`customer_id` = :customer_id '
        .      'ORDER BY `order_created` DESC '
        .      'LIMIT 1'
        . ') '
        . 'ORDER BY '
        .     '`product_name` ASC'
    ;

    require_once appPath() . 'database.php';
    $db = getDbConnection(); // Databaseconnectie openen.

    $stmt_orders = $db->prepare($sql_orders);
    if ($stmt_orders) {
        $customer_id = $_SESSION['customer']['id'];
        $stmt_orders->bindValue(':customer_id', $customer_id);
        if ($stmt_orders->execute()) {
            require_once appPath() . 'utilities.php';
            $exchangeRates = decodeJsonFromThisServer('/api/exchange_rates.json');
            while ($order = $stmt_orders->fetch()) {
                $priceSubtotal = $order['amount'] * $order['price'] * $exchangeRates[$order['currency']];
                $priceTotal += $priceSubtotal;
                $orders[] = [
                    'name'     => $order['name'],
                    'amount'   => $order['amount'],
                    'currency' => $order['currency'],
                    'price'    => $order['price'],
                    'subtotal' => $priceSubtotal,
                ];
            }
        }
    }

    $db = null; // Databaseconnectie sluiten.
} catch (PDOException $e) {
    $db = null; // Databaseconnectie sluiten.
    var_dump($e);
    exit;
}

?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Besteld</li>
    </ol>
    <div class="col-sm-offset-3 col-sm-9">
        <h1>Besteld</h1>
        <table class="table table-striped table-hover table-order">
            <thead>
                <tr>
                    <th scope="col">Product</th>
                    <th scope="col" class="text-right">Valuta</th>
                    <th scope="col" class="text-right">Stukprijs</th>
                    <th scope="col" class="text-right">Aantal</th>
                    <th scope="col" colspan="2" class="text-right">Subtotaal</th>
                </tr>
            </thead>
            <tbody>
<?php foreach($orders as $product): ?>
                <tr>
                    <td><?=$product['name'] ?></td>
                    <td class="text-right"><i class="glyphicon glyphicon-<?=code2Currency($product['currency']) ?>"></i></td>
                    <td class="text-right number"><?=price($product['price']) ?></td>
                    <td class="text-right number"><?=$product['amount'] ?></td>
                    <td class="text-right" width="1"><i class="glyphicon glyphicon-euro"></i></td>
                    <td class="text-right number" width="1"><?=price($product['subtotal']) ?></td>
                </tr>
<?php endforeach ?>
            </tbody>
            <tfoot>
                <tr>
                    <th scope="row" colspan="4" class="text-right">Totaal:</th>
                    <td class="text-right"><i class="glyphicon glyphicon-euro"></i></td>
                    <td class="text-right number"><strong><?=price($priceTotal) ?></strong></td>
                </tr>
            </tfoot>
        </table>
        <p><a href="?page=home" class="btn btn-link">Terug naar de startpagina</a></p>
    </div>
</div>
<?php include partialPath('footer') ?>
</body>
</html>