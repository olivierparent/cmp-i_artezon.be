<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

if (isset($_POST['btn-logout'])) {
    require_once appPath() . 'authentication.php';
    if (destroyAuthentication()) { // Functie uit app/authentication.php
        require_once appPath() . 'utilities.php';
        redirectTo('home'); // Functie uit app/utilities.php
    } else {
        $error = 'Kan niet afmelden.';
    }
}

?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Afmelden</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Meld je af</h1>
<?php if (isset($error)): ?>
    <div class="col-sm-offset-3 col-sm-9 alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="glyphicon glyphicon-warning-sign"></i> <?=$error ?>
    </div>
<?php endif ?>
    <p class="col-sm-offset-3 col-sm-9">Klik op de knop <strong>Afmelden</strong> om je af te melden.</p>
    <form action="<?=$_SERVER['REQUEST_URI'] ?>" class="form-horizontal" method="post">
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary" name="btn-logout"><i class="glyphicon glyphicon-log-out"></i> Afmelden</button>
                <a href="?page=home" class="btn btn-link">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<?php include partialPath('footer') ?>
</body>
</html>
