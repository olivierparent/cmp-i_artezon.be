<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once appPath() . 'authentication.php';
require_once appPath() . 'database.php';
require_once appPath() . 'utilities.php';

allowAuthenticatedOnly();

if (isset($_POST['btn-calculate']) && isset($_POST['amount'])) {
    foreach ($_POST['amount'] as $productCode => $productAmount) {
        $cookieName = "shoppingcart[{$productCode}]";
        setcookie($cookieName, $productAmount, time() + 3600 * 24 * 7);
    }

    redirectTo($_GET['page']);
}

$productsShoppingCart = [];
$productsSorted = [];
$priceTotal = 0;
if (isset($_COOKIE) && isset($_COOKIE['shoppingcart'])) {
    foreach ($_COOKIE['shoppingcart'] as $productCode => $productAmount) {
        $productsShoppingCart[$productCode] = (int) $productAmount;
        $sqlInClause[] = '?';
    }

    $sqlInClause = implode(',', $sqlInClause);
//    var_dump($sqlInClause);

    $db = getDbConnection();
    $sql_products
        = 'SELECT '
        .     '`product_id` AS `id`, '
        .     '`product_code` AS `code`, '
        .     '`product_name` AS `name`, '
        .     '`price_currency` AS `currency`, '
        .     '`price_value` AS `price` '
        . 'FROM `products` NATURAL JOIN `prices` '
        . 'WHERE '
        .     "`product_code` IN ({$sqlInClause}) "
        . 'ORDER BY '
        .     '`product_name` ASC'
    ;
//    var_dump($sql_products); exit;

    $stmt_products = $db->prepare($sql_products);
    if ($stmt_products) {
        $bind2Position = 1;
        foreach ($productsShoppingCart as $productCode => $productName) {
            $stmt_products->bindValue($bind2Position++, $productCode);
        }
        if ($stmt_products->execute()) {
            $exchangeRates = decodeJsonFromThisServer('/api/exchange_rates.json');
            while ($row_products = $stmt_products->fetch()) {
                $productAmount = $productsShoppingCart[$row_products['code']];
                $priceSubtotal = $row_products['price'] * $productAmount * $exchangeRates[$row_products['currency']];
                $priceTotal += $priceSubtotal;
                $productsSorted[$row_products['code']] = [
                    'id'       => $row_products['id'],
                    'name'     => $row_products['name'],
                    'currency' => $row_products['currency'],
                    'price'    => $row_products['price'],
                    'amount'   => $productAmount,
                    'subtotal' => $priceSubtotal,
                ];

            }
        }
    }

    $db = null; // Databaseconnectie sluiten.
}

if (isset($_POST['btn-order'])) {
    $db = getDbConnection();

    $sql_orders
        = 'INSERT INTO `orders` ('
        .     '`customer_id`'
        . ') VALUES ('
        .     ':customer_id'
        . ')'
    ;

    $stmt_orders = $db->prepare($sql_orders);
    if ($stmt_orders) {
        $customer_id = $_SESSION['customer']['id'];
        $stmt_orders->bindValue(':customer_id', $customer_id);
        $stmt_orders->execute();
        $order_id = $db->lastInsertId();

        $sql_orders_has_products
            = 'INSERT INTO `orders_has_products` ('
            .     '`order_id`, '
            .     '`product_id`, '
            .     '`product_amount`'
            . ') VALUES ('
            .     ':order_id,'
            .     ':product_id,'
            .     ':product_amount'
            . ')'
        ;
        $stmt_orders_has_products = $db->prepare($sql_orders_has_products);

        if ($stmt_orders_has_products) {
            foreach ($productsSorted as $productCode => $product) {
                $stmt_orders_has_products->bindValue(':order_id', $order_id);
                $stmt_orders_has_products->bindValue(':product_id', (int) $product['id']);
                $stmt_orders_has_products->bindValue(':product_amount', $product['amount']);
                $stmt_orders_has_products->execute();
                setcookie("shoppingcart[{$productCode}]", null, time() - 3600);
            }

        }
    }

    $db = null; // Databaseconnectie sluiten.

    redirectTo('order_placed');
}

/**
 * Als er geen producten in de bestelling zitten, stuur de customer naar de startpagina.
 */
if ($priceTotal < 0.01) {
    redirectTo('home');
}

?><!doctype html>
<html lang="nl">
<?php include partialPath('head') ?>
<body>
<div class="container">
    <ol class="breadcrumb hidden-sm hidden-xs">
        <li><a href="?page=home" title="Startpagina"><i class="glyphicon glyphicon-home"></i></a></li>
        <li class="active">Bestelling</li>
    </ol>
    <h1 class="col-sm-offset-3 col-sm-9">Bestelling</h1>
    <?php if (isset($error)): ?>
        <div class="col-sm-offset-3 col-sm-9 alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="glyphicon glyphicon-warning-sign"></i> <?=$error ?>
        </div>
    <?php endif ?>

    <form action="<?=$_SERVER['REQUEST_URI'] ?>" class="form-horizontal" method="post">
        <fieldset>
            <legend>Producten</legend>
            <div class="col-sm-offset-3 col-sm-9">
                <table class="table table-striped table-hover table-order">
                    <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col" class="text-right">Valuta</th>
                            <th scope="col" class="text-right">Stukprijs</th>
                            <th scope="col" class="text-right">Aantal</th>
                            <th scope="col" colspan="2" class="text-right">Subtotaal</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
<?php foreach($productsSorted as $productCode => $productDetails): ?>
                        <tr>
                            <th scope="row"><label for="input-<?=$productCode ?>"><?=$productDetails['name'] ?></label></th>
                            <td class="text-right number"><i class="glyphicon glyphicon-<?=code2Currency($productDetails['currency']) ?>"></i></td>
                            <td class="text-right number"><?=price($productDetails['price']) ?></td>
                            <td><input type="number" class="form-control text-right number" name="amount[<?=$productCode ?>]" id="input-<?=$productCode ?>" min="1" max="9" step="1" value="<?=$productDetails['amount'] ?>"></td>
                            <td class="text-right" width="1"><i class="glyphicon glyphicon-euro"></i></td>
                            <td class="text-right number" width="1"><?=price($productDetails['subtotal']) ?></td>
                            <td class="text-center"><a href="index.php?page=api&origin=<?=$_GET['page'] ?>&action=remove&product=<?=$productCode ?>" class="btn btn-default" title="Verwijderen 1 item"><i class="glyphicon glyphicon-remove"></i></a></td>
                        </tr>
<?php endforeach ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th scope="row" colspan="4" class="text-right">Totaal:</th>
                            <td class="text-right"><i class="glyphicon glyphicon-euro"></i></td>
                            <td class="text-right number"><strong><?=price($priceTotal) ?></strong></td>
                            <td class="text-center"><button type="submit" class="btn btn-default" name="btn-calculate" title="Herbereken totaal"><i class="glyphicon glyphicon-plus"></i></button></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary" name="btn-order"><i class="glyphicon glyphicon-gift"></i> Bestellen</button>
                <a href="?page=home" class="btn btn-link">Terug naar de startpagina</a>
            </div>
        </div>
    </form>
</div>
<?php include partialPath('footer') ?>
</body>
</html>