<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

/**
 * Toont een string met daarin de copyright boodschap.
 *
 * @return string
 */
function copyDate()
{
    global $config; // gebruikt NOOIT global variabelen, omdat je nooit weet wat er allemaal mee gebeurd is voordat ze hier gebruikt worden.

    $startDate   = $config['startDate'];
    $currentDate = (int) date('Y'); // date() geeft een string terug met het huidig jaartal, maar we willen een integer.

    return 'Copyright &copy; ' . ($startDate < $currentDate ? "{$startDate}-{$currentDate}" : $currentDate); // Ternaire operator: ( conditie ? als waar : als onwaar)
}

/**
 * Vervangt de inhoud van de variabele met een welkomstboodschap.
 *
 * Zie ook: http://php.net/implode
 * Zie ook: http://php.net/ucwords
 *
 * @param string $welcome Deze parameter is pass-by-reference (&): de originele variable die als argument meegegeven wordt zal gewijzigd worden in plaats van er eerst een kopie van te maken.
 * @param array $person
 */
function welcomeMessage(&$welcome, array $person = [])
{
    $welcome = ['Welkom'];

    if (empty($person)) {
        $welcome[] = 'bezoeker';
    } else {
        if (isset($person['givenname'])) {
            $welcome[] = ucwords($person['givenname']);
        }
        if (isset($person['familyname'])) {
            $welcome[] = ucwords($person['familyname']);
        }
    }

    $welcome = implode(' ', $welcome);
}

/**
 * Leest een XML-bestand in.
 *
 * Zie ook: http://php.net/file_exists
 * Zie ook: http://php.net/simplexml_load_file
 *
 * @param $path
 * @return bool|SimpleXMLElement
 */
function readXml($path)
{
    if (file_exists($path)) {
        return $xml = simplexml_load_file($path);
    }

    return false;
}

/**
 * Stuurt de browser naar een andere pagina. Let er op dat er vooraf geen output naar de browser gestuurd wordt, anders
 * is de HTTP-header al afgesloten, waardoor je er niets meer in kan veranderen.
 *
 * @param string $page
 */
function redirectTo($page)
{
    $uri = $_SERVER['HTTP_ORIGIN'] . $_SERVER['SCRIPT_NAME'] . '?page=' . $page;
    header("Location: {$uri}");
}

/**
 * Zet valutacode om in tekst.
 *
 * @param string $code
 * @return string
 */
function code2Currency($code)
{
    $currency = [
        'EUR' => 'euro',
        'GBP' => 'gbp',
        'USD' => 'usd',
    ];

    switch ($code) {
        case 'EUR':
        case 'GBP':
        case 'USD':
            return $currency[$code];
            break;
        default:
            return '';
            break;
    }
}

/**
 * Zet een float om in een correct gevormde prijs.
 *
 * @param float $float
 * @return string
 */
function price($float)
{
    return number_format($float, 2, ',', '.');
}

/**
 * Detecteer of een string die uit meer dan 4 karakters bestaat, enkel uppercase karakters bevat.
 * Minder dan 5 karakters zijn waarschijnlijk afkortingen.
 *
 * Zie: http://php.net/strlen
 * Zie: http://php.net/ctype_upper
 *
 * @param $string
 * @return bool
 */
function isUppercaseWord($string)
{
    return 4 < strlen($string) && ctype_upper($string);
}

/**
 * Zie: http://php.net/mb_convert_case
 *
 * @param $string
 * @return string
 */
function toLowerCase($string)
{
    return mb_convert_case($string, MB_CASE_TITLE, 'UTF-8');
}

/**
 * @param string $uri
 * @return mixed
 */
function decodeJsonFromThisServer($uri)
{
    $uriJson = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . $uri;
    $json = file_get_contents($uriJson);
    return json_decode($json, true);
}