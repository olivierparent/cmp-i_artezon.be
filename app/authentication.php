<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

require_once appPath() . 'security.php';

/**
 * De sessie moet gestart/herstart worden om de global variable $_SESSION te hebben.
 * Dit gebeurt in index.php
 *
 * Zie ook: http://php.net/session_start
 */
//session_start();

/**
 * Probeer de gebruiker te authentificeren. Indien gelukt, voeg een aantal gegevens toe aan de sessiearray.
 *
 * @param array $customer
 * @param $password
 * @return bool
 */
function attemptAuthentication(array $customer, $password) {
    if ($isAuthenticated = verifyPassword($password, $customer['password'])) {
        $_SESSION['authenticated-artezon'] = $isAuthenticated;
        $_SESSION['customer'] = [
            'id'         => (int) $customer['id'],
            'givenname'  => $customer['givenname'],
            'familyname' => $customer['familyname'],
        ];
    } else {
        destroyAuthentication();
    }

    return $isAuthenticated;
}

/**
 * Vernietigt de sessie onmiddellijk.
 *
 * Zie ook: http://php.net/session_destroy
 *
 * @return bool
 */
function destroyAuthentication()
{
    if (session_destroy()) {
        $_SESSION = []; // Omdat de voorgaande functie de sessie niet onmiddellijk leegmaakt.
        return true;
    }
    return false;
}

/**
 * Controleert of de gebruiker al dan niet aangemeld is.
 *
 * @return bool
 */
function isAuthenticated()
{
    return isset($_SESSION['authenticated-artezon']) ? $_SESSION['authenticated-artezon'] : false;
}

/**
 * Controleert of de gebruiker aangemeld is, zoniet wordt het script gestopt.
 */
function allowAuthenticatedOnly()
{
    if (!isset($_SESSION['authenticated-artezon']) || (isset($_SESSION['authenticated-artezon']) && !$_SESSION['authenticated-artezon'])) {
        die('Toegang verboden voor anonieme gebruikers. Meld je eerst aan.');
    }
}
