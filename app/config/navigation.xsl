<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" />
	<xsl:template match="/">
		<navigation>
			<xsl:for-each select="navigation/item">
				<xsl:sort select="@id" order="ascending"/>
				<item>
					<label><xsl:value-of select="label" /></label>
					<page><xsl:value-of select="page" /></page>
					<cagtegory><xsl:value-of select="category" /></cagtegory>
				</item>
			</xsl:for-each>
		</navigation>
	</xsl:template>
</xsl:stylesheet>