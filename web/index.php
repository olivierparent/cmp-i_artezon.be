<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

/**
 * De sessie moet gestart/herstart worden om de global variable $_SESSION te hebben.
 */
session_start();

/**
 * Geeft het absoluut pad naar de map waarin de applicatie staat terug.
 *
 * @param array $relativePath
 * @return string
 */
function appPath(array $relativePath = [])
{
    $path = [
        __DIR__, // pad naar dit bestand
        '..',    // parent-folder
        'app',   // 'app' folder
    ];

    if (!empty($relativePath)) {
        $path = array_merge($path, $relativePath);
    }

    /**
     * Zie: http://php.net/implode
     * Zie: http://php.net/realpath
     */
    $path = realpath(implode($path, DIRECTORY_SEPARATOR)) . DIRECTORY_SEPARATOR;

    return $path;
}

/**
 * Geeft het absoluut pad naar het PHP-bestand van een pagina terug.
 *
 * @param $pageName
 * @return bool|string Absoluut pad naar het PHP-bestand van een pagina terug.
 */
function pagePath($pageName)
{
    $pagePath = appPath(['pages']) . $pageName . '.php';

    return file_exists($pagePath) ? $pagePath : false;
}

/**
 * Geeft het absoluut pad naar het PHTML-bestand van een partial (gedeeltelijke pagina) terug.
 *
 * @param $pagePartialName
 * @return bool|string Absoluut pad naar het PHTML-bestand van een partial (gedeeltelijke pagina) terug.
 */
function partialPath($pagePartialName)
{
    $partialPath = appPath(['pages', 'partials']) . $pagePartialName . '.phtml';

    return file_exists($partialPath) ? $partialPath : false;
}

/**
 * Lees het configuratiestand in. Vang de array op in de variable $config.
 */
$config = require_once appPath(['config']) . 'config.php';

/**
 * Paginanaam uit de GET-variabele met de naam 'page' halen (URL: ?page=paginanaam)
 * Indien er een GET-variabele met de naam 'page' is, gebruik de waarde, zoniet gebruik 'home'.
 */
$pageName = isset($_GET['page']) ? $_GET['page'] : 'home';
if ($page = pagePath($pageName)) {
    require $page;
} else {
    $error = [
        'title'   => 'Fout 404',
        'message' => "De pagina '{$_GET['page']}' kon niet gevonden worden.",
    ];

    /**
     * Zie: http://php.net/header
     */
    header('HTTP/1.0 404 Not Found');
    require pagePath('error');
}
