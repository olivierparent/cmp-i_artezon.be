	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 *                                                                           *
	 *                                                                           *
	 *                                                                           *
	 *                        aaaAAaaa            HHHHHH                         *
	 *                     aaAAAAAAAAAAaa         HHHHHH                         *
	 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
	 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
	 *                   aAAAAAa    aAAAAAA                                      *
	 *                   AAAAAa      AAAAAA                                      *
	 *                   AAAAAa      AAAAAA                                      *
	 *                   aAAAAAa     AAAAAA                                      *
	 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
	 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
	 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
	 *                         aaAAAAAAAAAA       HHHHHH                         *
	 *                                                                           *
	 *                                                                           *
	 *                                                                           *
	 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
	 *                                                                           *
	 *                                                                           *
	 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
	 *                                                                           *
	 *                                                                           *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	 Olivier Parent
	 Copyright © 2013 Artevelde University College Ghent


Crossmedia Publishing I - Artezon.be
====================================
Webwinkel als introductie tot (niet-Objectgeoriënteerde) PHP 5.4

Installatieinstructies
----------------------

Download als zip en decomprimeer in een map op de server. Of gebruik git:

    $ git clone https://bitbucket.org/olivierparent/cmp-i_artezon.be CMP-I_artezon.be
    $ cd CMP-I_artezon.be

### Database

Maak een database aan door het EER Model uit het bestand `docs/CMP-I_database.mwb` te Forward Engineeren. Pas zo nodig de configuratie aan die je vindt in het bestand `app/config/database.ini`

### Registreren en aanmelden

Registreer eerst via de homepagina. De onderstaande URL's zijn voor BitNami MAMP/WAMP Stack met de standaardpoorten.

Mac OS X (server moet je eerst manueel starten!):

    http://localhost:8080/CMP-I_artezon.be/web/
    http://localhost:8080/CMP-I_artezon.be/web/?page=home

Windows:

    http://localhost/CMP-I_artezon.be/web/
    http://localhost/CMP-I_artezon.be/web/?page=home

### Gegevens toevoegen aan de database

Na de registratie kan je aanmelden en gegevens invoeren via de knop __Beheer__ in de footer van de pagina.

* Postcodes vind je in het bestand `docs/zipcodes_num_nl.csv`
* Producten vind je in het bestand `data/products.xml`